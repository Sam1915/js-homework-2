let userName = prompt('What is your name ?');
while (isFinite(userName)) {
    userName = prompt('What is your name ?');
}
console.log(`Your name is ${userName}`);

let userAge = +prompt('How old are you ?');
while (isNaN(userAge)) {
    userAge = +prompt('How old are you ?');
}
console.log(`You're ${userAge} years old`);

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    confirm('Are you sure you want to continue?')
        ? alert(`Welcome, ${userName}`)
        : alert('You are not allowed to visit this website');
} else {
    alert(`Welcome, ${userName}`);
}